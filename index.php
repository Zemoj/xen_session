<?php

require ('xen2_session.class.php');

$temp = new xen2_session();

if($temp->isValidUser()){
    echo "Welcome ".$temp->username()."!<br/>\n";

    if($temp->isMemberOfGroup(3)){
        echo "You are in group 3.<br/>\n";
    }else{
        echo "Problem with group 3.<br/>\n";
    }

    if($temp->isMemberOfGroups(array(8,4))){
        echo "You are in group 8 or 4.<br/>\n";
    }else{
        echo "Problem with group 8 or 4.<br/>\n";
    }


}else{
    echo "You are not logged in...";
}

$user=$temp->dump();



echo "<br/>\n<br/>\n";
//var_dump($user['secondary_group_ids']);
//var_dump($user);

?>