<?php

class xen2_session{

    protected $user;
    protected $custom_fields;//FIX

    public function __construct(){
        //Xenforo install location
        $fileDir = '/home/z_horizon/public_html/forums';
        
        //Xenforo Session Setup
        require ($fileDir . '/src/XF.php');
        XF::start(__DIR__);
        $app = XF::setupApp('XF\Pub\App');
        $app->start();
        
        $this->user = XF::visitor();

    }

    //Check for a valid user ID
    public function isValidUser(){
        if (isset($this->user['user_id']) && $this->user['user_id'] > 0){return true;}else{return false;}
    }

    //Return User ID or false
    public function userid(){
        if(!$this->isValidUser()){return false;}
        return $this->user['user_id'];
    }

    //Return Username or false
    public function username(){
        if(!$this->isValidUser()){return false;}
        return $this->user['username'];
    }

    //Check if user is a member of group (ID)
    public function isMemberOfGroup($group){
        if(!$this->isValidUser()){return false;}
        return in_array($group, $this->user['secondary_group_ids']);
    }

    //Check if a user is a member of atleast 1 group in array of group ID's
    public function isMemberOfGroups($groups){
        if(!$this->isValidUser()){return false;}
        if(count(array_intersect($groups, $this->user['secondary_group_ids'])) >0){return true;}else{return false;}
    }
    
    
    //Get value of a Xenforo Custom Field by field ID


    //Get value of current forum theme


    public function dump(){
        return $this->user;
    }
}

?>