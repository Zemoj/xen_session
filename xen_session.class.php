<?php

class xen_session{

    protected $user;
    protected $visitor;
    protected $custom_fields;

    public function __construct(){
        //Xenforo install location
        $fileDir = '/var/www/public_html/forums';
        
        //Xenforo Session Setup
        require ($fileDir . '/library/XenForo/Autoloader.php');
        XenForo_Autoloader::getInstance()->setupAutoloader($fileDir . '/library');
        XenForo_Application::initialize($fileDir . '/library/', $fileDir);
        $request = new Zend_Controller_Request_Http();
        $request->setBasePath('/community');
        $session = XenForo_Session::startPublicSession($request);
        $userModel = XenForo_Model::create('XenForo_Model_User');
        $this->user = $userModel->getUserById($session->get('user_id'));
        $this->visitor = XenForo_Visitor::getInstance()->toArray();
        $this->custom_fields = unserialize($this->visitor['custom_fields']);
    }

    //Check for a valid user ID
    public function isValidUser(){
        if (isset($this->user['user_id']) && $this->user['user_id'] > 0){return true;}else{return false;}
    }

    //Check if user is a member of group (ID)
    public function isMemberOfGroup($group){
        if(!$this->isValidUser()){return false;}
        return in_array($group, explode(",", $this->user['secondary_group_ids']));
    }

    //Check if a user is a member of atleast 1 group in array of group ID's
    public function isMemberOfGroups($groups){
        if(!$this->isValidUser()){return false;}
        if(count(array_intersect($groups, explode(",", $this->user['secondary_group_ids']))) >0){return true;}else{return false;}
    }
    
    //Return Username or false
    public function username(){
        if(!$this->isValidUser()){return false;}
        return $this->user['username'];
    }
    
    //Return User ID or false
    public function userid(){
        if(!$this->isValidUser()){return false;}
        return $this->user['user_id'];
    }
    
    //Get value of a Xenforo Custom Field by field ID
    public function getCustomField($f){
        if(!$this->isValidUser()){return false;}
        if(isset($this->custom_fields) && isset($this->custom_fields[$f])){
            return $this->custom_fields[$f];
        }else{return false;}
    }  

    //Get value of current forum theme
    public function getStyleID(){
        if(!$this->isValidUser()){return false;}
        if(isset($this->user['style_id'])){return $this->user['style_id'];}else{return false;}
    }
}

?>